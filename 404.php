<!DOCTYPE html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Citsa - 404</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic'>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu" rel="stylesheet">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="static/css/bootstrap.css">
    <!-- main css -->
	<link rel="stylesheet" type="text/css" href="static/css/404.css">
    <script src="static/js/modernizr-2.6.2.min.js"></script>
</head>
<body>
<div class="ff">404</div>

<div class="box text-center">
    <div>
        close !
    </div>
    <p class="text-center">
    	<span>error 404 !</span>
    	page isn't found for one of the reformes 
    </p>
</div>
</body>
</html>


