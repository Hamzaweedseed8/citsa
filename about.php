<?php 
    require_once 'core/db.php';
    include 'includes/head.php';
    include 'includes/nav.php';
?>
	<!-- Landing page -->
	<section id="citsa-home" data-section="home" style="background-image: url(static/images/bg-img/page.jpg);">
		<!-- <div class="gradient"></div> -->
		<div class="container">
			<div class="text-wrap">
				<div class="text-inner">
					<div class="row">
						<div class="col-md-12">
							<h1 class="to-animate">About Us</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slant"></div>
	</section>
	<!-- end of landing page -->

	<!-- About section -->
	<section id="citsa-about" class="citsa-about" data-section="about">
		<div class="container">
			<div class="row row-bottom-padded-lg">
				<!-- introduction card 01 -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="citsa-block to-animate" id="mission">
						<div class="citsa-text">
							<h2> <i class="icon-bulb"></i> Mission</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis id, in ad, eum, laudantium iure labore, voluptas qui itaque nisi ullam? Distinctio, officiis, dolore. Fugit, eligendi dignissimos. Laudantium non, temporibus!</p>
						</div>
					</div>
				</div>
				<!-- end of introduction card 01 -->

				<!-- introduction card 02 -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="citsa-block to-animate">
						<div class="citsa-text">
							<h2><i class="icon-wrench"></i> Objectives</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima, reprehenderit, amet. Commodi nisi, necessitatibus ab eaque in quod eum incidunt neque fuga aut? Facere laudantium fugit recusandae a, iusto culpa!</p>
						</div>
					</div>
				</div>
				<!-- end of introduction card 02 -->

				<!-- introduction card 03 -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="citsa-block to-animate">
						<div class="citsa-text">
							<h2><i class="icon-rocket"></i> vision</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum tempora atque a necessitatibus odio labore molestiae rem dicta, culpa doloremque, delectus blanditiis velit. Quae dolores aperiam obcaecati molestiae enim nobis?</p>
						</div>
					</div>
				</div>
				<!-- end of introduction card 03 -->
			</div>
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">About Us</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center to-animate">
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, iusto! Velit officiis nesciunt minus laudantium excepturi sunt aut, magni qui tempore debitis provident? Veniam eius voluptates cupiditate corporis a libero.	
						Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vero similique beatae inventore iste natus nemo quasi delectus temporibus nulla porro quo minus asperiores eum, tempore, itaque ut quidem, recusandae quia.
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Distinctio cupiditate excepturi itaque reiciendis soluta optio, voluptatem, nostrum quis vitae harum iusto praesentium architecto autem ipsa in dolorem nobis aliquam ducimus.
						Lorem ipsum, dolor sit amet consectetur adipisicing elit. Hic voluptatum repudiandae, cumque, excepturi velit praesentium et ad magni dolor eos asperiores quisquam eligendi in eaque non cum. Quas, porro accusamus?
					</p>
				</div>
			</div>
		</div>
	</section>
	<!-- end of about section -->

	


	
<?php 
	include 'includes/footer.php';

	?>
