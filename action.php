<?php	
session_start(); 
require_once 'core/db.php';

if(isset($_POST["search"])){
	$keyword = $_POST["library"];
	$sql = "SELECT * FROM library WHERE keyword LIKE '%$keyword%'";
	$run_query = mysqli_query($db,$sql);
	?>
	<section id="fh5co-contact" data-section="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 text-center" ">
				<h3>Results on &ldquo; <span style="color:#3f95ea ;font-style: italic;"><?=$keyword;?></span> &ldquo;</h3>
			</div>
		</div>
		<div class="row row-bottom-padded-md">
			<div class="col-md-12 library-result"> 
				<ul>
					<?php
						if(mysqli_num_rows($run_query) > 0){
							$no = 1;
							while($row=mysqli_fetch_array($run_query)){
								$lib_id    = $row['id'];
								$lib_code  = $row['code'];
								$lib_title = $row['title'];
								$lib_download = urlencode($row['file']);
								echo "
									<li>
										<span style='margin-right:20px;'>$no.</span>
										<span class='filename'>$lib_code - $lib_title</span>
										<span class='pull-right'>
										<a href='download.php?view=$lib_download' pid='$lib_id'><i class='fa fa-file'></i></a> 
										<a href='download.php?file=$lib_download'><i class='fa fa-download' pid='$lib_id'></i></a>
										</span>
									</li>
								";
								$no = $no + 1;
							}
						}
						?>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<?php
}
if(isset($_POST["awaitingevent"])){
	$query = "SELECT * FROM events WHERE id != 0 ORDER BY date_of_event ASC  LIMIT 1";
    $run_query = mysqli_query($db,$query) or die(mysqli_error($db));
    if(mysqli_num_rows($run_query) > 0){
        while($row = mysqli_fetch_array($run_query)){
            $id    = $row['id'];
            $host   = $row['host'];
            $title = $row['title'];
            $date = formatDateTime($row['date_of_event']);
            $image = $row['image'];
            $about = $row['event_about'];
			echo "
				
			";
		}
	}
}
if(isset($_POST["latestevent"])){
	$awaitingquery = "SELECT * FROM events WHERE id != 0 ORDER BY date_of_event ASC  LIMIT 1";
    $run_awaitingquery = mysqli_query($db,$awaitingquery) or die(mysqli_error($db));
    $row = mysqli_fetch_array($run_awaitingquery);
    $awaiting_id    = $row['id'];

	$query = "SELECT * FROM events WHERE id != '$awaiting_id' ORDER BY date_of_event ASC  LIMIT 3";
    $run_query = mysqli_query($db,$query) or die(mysqli_error($db));
    if(mysqli_num_rows($run_query) > 0){
        while($row = mysqli_fetch_array($run_query)){
            $id    = $row['id'];
            $host   = $row['host'];
            $title = $row['title'];
            $date = formatDateTime($row['date_of_event']);
            $image = $row['image'];
			echo "
				
			";
		}
	}
}



if(isset($_POST["awaitingnews"])){
	$query = "SELECT * FROM new WHERE news_id != '' ORDER BY date_added ASC  LIMIT 1";
	$run_query = mysqli_query($db,$query);
	if(mysqli_num_rows($run_query) > 0){
		while($row = mysqli_fetch_array($run_query)){
			$id = $row['news_id'];
			$news_title = $row['news_title'];
			$news_description = $row['news_description'];
			$news_reporter = $row['news_reporter'];
			$news_date = formatDate($row['date_added']);
			echo "
			";
		}
	}
}


if(isset($_POST["oldnews"])){
	$oldquery = "SELECT * FROM new WHERE news_id != 0 ORDER BY date_added ASC  LIMIT 1";
    $run_oldquery = mysqli_query($db,$oldquery) or die(mysqli_error($db));
    $row = mysqli_fetch_array($run_oldquery);
    $old_id    = $row['news_id'];

	$query = "SELECT * FROM new WHERE news_id != '$old_id' ORDER BY date_added ASC  LIMIT 3";
    $run_query = mysqli_query($db,$query) or die(mysqli_error($db));
    if(mysqli_num_rows($run_query) > 0){
        while($row = mysqli_fetch_array($run_query)){
            $id = $row['news_id'];
			$news_title = $row['news_title'];
			$news_description = $row['news_description'];
			$news_reporter = $row['news_reporter'];
			$news_date = formatDate($row['date_added']);
			echo "
				
				
			";
		}
	}
}


?>
<script>
 var strn = document.getElementById("about").innerHTML; 
   var rest = strn.slice(0, 400);
    document.getElementById("about").innerHTML = rest + " ...";
    </script>