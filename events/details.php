<?php
    require_once '../core/db.php';
    include '../includes/head.php';
    include '../includes/nav.php';


	if(isset($_GET['detail'])){
    $event_id =$_GET['detail'];
    $event_id = sanitize($event_id);
  	}else{
        header('Location: index.php');
  	}

	$sql = "SELECT * FROM events WHERE id = '$event_id' ";
  	$query = $db->query($sql);
	$row = mysqli_fetch_array($query); 
?>
    <section id="citsa-home" data-section="home" style="background-image: url(<?=$row['image'];?>);">
    	<div class="gradient"></div>
    	<div class="container">
    		<div class="text-wrap">
    			<div class="text-inner">
    				<div class="row">
    					<div class="col-md-8 col-md-offset-2 text-center">
    						<h1 class="to-animate"><?=$row['title'];?></h1>
                            <!-- <p class="to-animate" >4 September 2018 Sasakawa Resturant</p> -->
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	<div class="slant"></div>
    </section>
    <section class="pt100 pb100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xs-6 col-md-3  ">
                    <div class="icon_box_two">
                        <i class="fa fa-calendar-o" aria-hidden="true"></i>
                        <div class="content">
                            <h5 class="box_title">DATE</h5>
                            <p><?=formatDateTime($row['date_of_event']);?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-3 ">
                    <div class="icon_box_two">
                        <i class="icon-map-marker" aria-hidden="true"></i>
                        <div class="content">
                            <h5 class="box_title">location</h5>
                            <p><?=$row['location'];?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-3  ">
                    <div class="icon_box_two">
                        <i class="icon-user"></i>
                        <div class="content">
                            <h5 class="box_title">
                                speakers
                            </h5>
                            <p><?=$row['host'];?></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-3  ">
                    <div class="icon_box_two">
                        <i class="icon-tag"></i>
                        <div class="content">
                            <h5 class="box_title">tikets</h5>
                            <p>free</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!--about the event -->
    <section id="citsa-contact" data-section="" class="pt100 pb100">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center">
                    <h2 class="to-animate">About the event</h2>
               </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-12">
                    <p class="to-animate">
                        <?=$row['event_about'];?>
                        <?php
                        //  $count = strlen($row['event_about']);
                        //        $div = ($count / 2);
                        //        echo $div;
                        ?>
                    </p>
                </div>
                <!-- <div class="col-12 col-md-6">
                    <p class="to-animate">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio deserunt vero accusantium quasi itaque incidunt, est suscipit, quo, doloremque cupiditate hic, facere cumque obcaecati soluta ea unde! Voluptatem, aperiam, dolor!
                    </p>
                </div> -->
            </div>
        </div>
    </section>
    <section id="citsa-library"  style="background-image: url(../static/images/bg-img/11.jpg);">
        <div class="countergradient"></div>
        <div class="container">
            <div class="text-wrap">
                <div class="text-inner">
                    <div class="row">
                        <div class="col-md-12 countdown text-center">
                            <h2 class="">Counter Until The Big Event</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div id="countdown">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--about the event end -->
    <?php //if($row['host'] != ' '){ ?>
    <!-- <section id="citsa-work" data-section="albums">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center">
                    <h2 class="to-animate">our speakers</h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 subtext to-animate">
                            <h3></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center no-gutters  row-bottom-padded-sm">
                <div class="col-md-3 col-sm-6">
                    <div class="speaker_box to-animate">
                        <div class="speaker_img">
                            <img src="../static/images/1.jpg" alt="speaker name">
                            <div class="info_box to-animate">
                                <h5 class="name">Patricia Stone</h5>
                                <p class="position">CEO Company</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <?php //} ?>
    <!--speaker section end -->
    <?php 
    $prev_id =$row['id'] ;
    $Csql = "SELECT * FROM events WHERE id != '{$prev_id}' ORDER BY date_of_event LIMIT 5";
    $Cquery = $db->query($Csql);
    ?>
    <section class="pb100">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-headng text-center">
                    <h2 class="to-animate"></h2>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 subtext to-animate">
                            <h3></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-bottom-padded-sm">
                <div class="table-responsive">
                    <div class="table-responsive">
                        <table class="table event_calender">
                            <thead class="event_title">
                                <tr>
                                    <th><i class="fa fa-calendar-o" aria-hidden="true"></i> <span>next events calendar</span></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                        <tbody>
                        <?php 
                            while($Crow = mysqli_fetch_array($Cquery)){
                        ?>
                            <tr>
                                <td><img src="<?=$Crow['image'];?>" alt="event"></td>
                                <td class="event_date"><?=formatDay($Crow['date_of_event']);?><span><?=formatMonth($Crow['date_of_event']);?></span></td>
                                <td>
                                    <div class="event_place">
                                        <h5 class="h5"><?=$Crow['title'];?></h5>
                                        <h6 class="h6"><?=formatTime($Crow['date_of_event']);?></h6>
                                        <p>Speaker: <?=$Crow['host'];?></p>
                                    </div>
                                </td>
                                <td><a href="details.php?detail=<?=$Crow['id'];?>" class="btn btn-primary btn-round btn-shine">Read More</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <!--get tickets section -->
<!-- <section id="citsa-ticket" style="background-image: url(../static/images/11.jpg);" data-section=""> 
    <div class="container">
        <div class="row">
            <div class="col-md-12 section-heading text-center">
                <h2 class="">Get your ticket</h2>
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-md-9 text-left">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus massa nec gravida tempus. Integer iaculis in aazzzCurabitur a pulvinar nunc. Maecenas laoreet finibus lectus, at volutpat ligula euismod.
            </div>
            <div class="col-md-3 text-center">
                <a href="#" class="btn btn-primary btn-round btn-shine">buy now</a>
            </div>
        </div>
    </div>
</section> -->

<?php 
include '../includes/footer.php'; 
?>
    <script type="">
        var countdown = document.querySelector('#countdown');
        //set launch Date(ms)
        var launchDate = new Date('<?=formatDateTime($row['date_of_event']);?>').getTime();
        //update every second
        var intvl = setInterval(() => {
            //Get todays date and time (ms)
            var now = new Date().getTime();
            //Distance from now to the launch date
            var distance = launchDate - now;
            //Time Calculation
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var mins = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            //Display result
            countdown.innerHTML = `
                <div class="">${days}<span>Days</span></div>
                <div class="">${hours}<span>Hours</span></div>
                <div class="">${mins}<span>Minutes</span></div>
                <div class="">${seconds}<span>Seconds</span></div>
            `;
            //If launch date Passed
            if(distance < 0){
                //Stop countdown
                clearInterval(intvl);
                //style and output text
                countdown.style.color = '#fff';
                countdown.innerHTML = 'Launched!';
            }
        }, 1000);
</script>