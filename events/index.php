<?php 
    require_once '../core/db.php';
    include '../includes/head.php';
    include '../includes/nav.php';
?>

    <section id="citsa-home" data-section="home" >
        <aside id="citsa-hero" class="js-fullheight">
            <div class="flexslider js-fullheight">
                <ul class="slides">
                    <?php 
                        $sql = "SELECT * FROM events WHERE id != '' ORDER BY date_of_event LIMIT 3";
                        $query = $db->query($sql);
                        while($row = mysqli_fetch_array($query)){
                    ?>
                        <li style="background-image: url(<?=$row['bg_img'];?>);">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-md-12 text-center js-fullheight slider-text">
                                        <div class="slider-text-inner">
                                            <h1><?=$row['title'];?></h1>
                                            <p><?=formatDateTime($row['date_of_event']);?>  <span><?=$row['location'];?></span></p>
                                            <p><a class="btn btn-primary btn-round btn-shine" href="details.php?detail=<?=$row['id'];?>">see details</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </aside>
    	<div class="slant"></div>
    </section>

    <!-- event section -->
    <?php 
                        $sql = "SELECT * FROM events WHERE id != '' ORDER BY date_of_event ";
                        $query = $db->query($sql);
                      
                    ?>
    <section id="citsa-testimonials" data-section="events">
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-heading text-center">
                    <h2 class="to-animate">ALL EVENTS</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-7">
                <?php 
                 $sql = "SELECT * FROM events WHERE id != '' ORDER BY date_of_event ";
                 $query = $db->query($sql);
                  while($row = mysqli_fetch_array($query)){
                    $id = $row['id'];
                    $id = $id%2;
                      if( $id == 0){
                ?>
                    <div class="blog-posts-area col-md-12">
                        <div class="single-blog-post featured-post single-post">
                            <div class="post-thumb">
                                <a href="details.php?detail=<?=$row['id'];?>">
                                    <img src="<?=$row['bg_img'];?>" alt="">
                                </a>
                            </div>
                            <div class="post-data">
                                <span class="btn btn-date btn-default"><?=formatDateTime($row['date_of_event']);?></span>
                                <a href="details.php?detail=<?=$row['id'];?>" class="post-title">
                                    <h6 style="text-transform:capitalize"><?=$row['title'];?></h6>
                                </a>
                                <div class="post-meta">
                                    <!-- <p class="post-author">Host <span>Christinne Williams</span></p> -->
                                    <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placerat. Sed varius leo ac leo fermentum, eu cursus nunc maximus. Integer convallis nisi nibh, et ornare neque ullamcorper ac. Nam id congue lectus, a venenatis massa. Maecenas justo libero, vulputate vel nunc id, blandit feugiat sem.</p>
                                </div>
                                <a href="details.php?detail=<?=$row['id'];?>" class="btn btn-readmore btn-round btn-shine"> Read More</a>
                            </div>
                        </div>
                    </div>
                  <?php }
                 }?>
                </div>
                <div class="col-12 col-lg-5">
                <?php 
                 $sql = "SELECT * FROM events WHERE id != '' ORDER BY date_of_event ";
                 $query = $db->query($sql);
                  while($row = mysqli_fetch_array($query)){
                    $id = $row['id'];
                    $id = $id%2;
                      if( $id != 0){
                ?>
                    <div class="blog-posts-area col-md-12">
                        <div class="single-blog-post featured-post single-post">
                            <div class="post-thumb">
                                <a href="details.php?detail=<?=$row['id'];?>">
                                    <img src="<?=$row['bg_img'];?>" alt="">
                                </a>
                            </div>
                            <div class="post-data">
                                <span class="btn btn-date btn-default"><?=formatDateTime($row['date_of_event']);?></span>
                                <a href="details.php?detail=<?=$row['id'];?>" class="post-title">
                                    <h6 style="text-transform:capitalize"><?=$row['title'];?></h6>
                                </a>
                                <div class="post-meta">
                                    <!-- <p class="post-author">Host <span>Christinne Williams</span></p> -->
                                    <p class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu metus sit amet odio sodales placerat. Sed varius leo ac leo fermentum, eu cursus nunc maximus. Integer convallis nisi nibh, et ornare neque ullamcorper ac. Nam id congue lectus, a venenatis massa. Maecenas justo libero, vulputate vel nunc id, blandit feugiat sem.</p>
                                </div>
                                <a href="details.php?detail=<?=$row['id'];?>" class="btn btn-readmore btn-round"> Read More</a>
                            </div>
                        </div>
                    </div>
                    <?php }
                 }?>
                </div>
            </div>
        </div>
    </section>



    <?php 
    include '../includes/footer.php';
?>

    <script src="../static/js/jquery.flexslider-min.js"></script>
    <script src="../static/js/slide.js"></script>
  