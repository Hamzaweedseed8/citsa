
	<!-- footer section -->
	<section id="citsa-footer" data-section="contact">
		<div class="slantOp"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 colsm-4 col-xs-12">
					<div class="row">
						<div class="col-md-12 section-heading-2 text-center">
							<h2 class="">About Us</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-12">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed cumque aliquid assumenda expedita  magni quibusdam, iure quidem!</p></div>
					</div>
					<div class="row">
						<div class="pull-left col-md-12 col-12">
							<p><a href="" class="btn btn-primary btn-round btn-shine">Read More</a></p>
						</div>
					</div>
				</div>
				<div class="col-md-4 colsm-4 col-xs-12">
					<div class="row">
						<div class="col-md-12 section-heading-2 text-center">
							<h2 class="">Locate Us</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-12 col-md-12">
							<ul class="locate">
								<li>
									<i class="icon-map-marker"></i> University Of Cape Coast - Ghana
								</li>
								<li><i class="icon-phone"></i>  +233557566646</li>
								<li>
									<a href="mailto:info@ucccitsa.com"> <i class="icon-envelope"></i> info@ucccitsa.ml</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-4 colsm-4 col-xs-12">
					<div class="row">
						<div class="col-md-12 section-heading-2 text-center">
							<h2 class="">Follow Us</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-12 text-center">
							<ul class="social social-circle">
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-instagram"></i></a></li>
								<li><a href="#"><i class="icon-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div> 
			</div>
		</div>
	</section> 
	<!-- end of footer section -->

	<!-- bottom footer -->
	<footer id="footer" role="contentinfo">
		<a href="#" class="gotop js-gotop"><i class="icon-arrow-up2"></i></a>
		<div class="container">
				<div class="col-md-12 text-center">
					<p>&copy;<script>
						document.write(new Date().getFullYear())
					</script> Citsa - UCC. All Rights Reserved. <br>Created by <a href="#" target="_blank">Team Anonymous.</a></p>

			</div>
		</div>
	</footer>
	<!--enf of bottom footer  -->


	
	<!-- jQuery -->
	<script src="http://localhost/citsa-pwa/static/js/jquery.min.js"></script>
	<script src="http://localhost/citsa-pwa/action.js"></script>
	<!-- jQuery Easing -->
	<script src="http://localhost/citsa-pwa/static/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="http://localhost/citsa-pwa/static/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="http://localhost/citsa-pwa/static/js/jquery.waypoints.min.js"></script>
	<script src="http://localhost/citsa-pwa/static/js/jquery.magnific-popup.min.js"></script>
	<script src="http://localhost/citsa-pwa/static/js/magnific-popup-options.js"></script>
	<!-- Stellar Parallax -->
	<script src="http://localhost/citsa-pwa/static/js/jquery.stellar.min.js"></script>
	<script src="http://localhost/citsa-pwa/static/js/typeahead.min.js"></script>
	<script src="http://localhost/citsa-pwa/static/js/lightbox.min.js"></script>
    <script src="http://localhost/citsa-pwa/static/js/swiper.min.js"></script>
	<script src="http://localhost/citsa-pwa/static/js/main.js"></script>
</body>
</html>