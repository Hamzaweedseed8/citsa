<!DOCTYPE html>
<html class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Citsa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic,700italic|Merriweather:300,400italic,300italic,400,700italic'>
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu" rel="stylesheet">
	<!-- Animate.css -->
	<link rel="stylesheet" href="http://localhost/citsa-pwa/static/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="http://localhost/citsa-pwa/static/css/icomoon.css">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="http://localhost/citsa-pwa/static/css/simple-line-icons.css">
	<!-- Bootstrap  -->
	<!-- Magnific Popup -->
	<link rel="stylesheet" href="http://localhost/citsa-pwa/static/css/magnific-popup.css">
	<link rel="stylesheet" href="http://localhost/citsa-pwa/static/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="http://localhost/citsa-pwa/static/css/lightbox.css">
	<link rel="stylesheet" href="http://localhost/citsa-pwa/static/css/swiper.min.css">
	<!-- main css -->
    <link rel="stylesheet"  href="http://localhost/citsa-pwa/static/css/style.css">	
    <link rel="stylesheet"  href="http://localhost/citsa-pwa/static/css/details.css">

	<script src="http://localhost/citsa-pwa/static/js/modernizr-2.6.2.min.js"></script>
</head>
<body>