
	<!-- About section -->
	<section id="citsa-about" data-section="about">
		<div class="container">
			<div class="row row-bottom-padded-lg no-gutters">
				<!-- introduction card 01 -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="citsa-block to-animate" style="">
						<div class="overlay-darker"></div>
						<div class="overlay"></div>
						<div class="citsa-text">
							<i class="citsa-about-icon icon-bulb"></i>
							<h2>Mission</h2>
							<p><a href="about#mission" class="btn btn-primary btn-round btn-shine">Get In Touch</a></p>
						</div>
					</div>
				</div>
				<!-- end of introduction card 01 -->

				<!-- introduction card 02 -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="citsa-block to-animate" style="background-image: url(static/images/pic13.JPG);">
						<div class="overlay-darker"></div>
						<div class="overlay"></div>
						<div class="citsa-text">
							<i class="citsa-about-icon icon-wrench"></i>
							<h2>Objectives</h2>
							<p></p>
							<p><a href="about#objectives" class="btn btn-primary btn-round btn-shine">Click Me</a></p>
						</div>
					</div>
				</div>
				<!-- end of introduction card 02 -->

				<!-- introduction card 03 -->
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="citsa-block to-animate" style="">
						<div class="overlay-darker"></div>
						<div class="overlay"></div>
						<div class="citsa-text">
							<i class="citsa-about-icon icon-rocket"></i>
							<h2>vision</h2>
							<p></p>
							<p><a href="about#vision" class="btn btn-primary btn-round btn-shine">Why Us?</a></p>
						</div>
					</div>
				</div>
				<!-- end of introduction card 03 -->
			</div>
			<!-- button to link to about page -->
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">About Us</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center to-animate">
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, iusto! Velit officiis nesciunt minus laudantium excepturi sunt aut, magni qui tempore debitis provident? Veniam eius voluptates cupiditate corporis a libero.	
						Lorem ipsum dolor, sit amet consectetur adipisicing elit. Vero similique beatae inventore iste natus nemo quasi delectus temporibus nulla porro quo minus asperiores eum, tempore, itaque ut quidem, recusandae quia.
						
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center to-animate">
					<p><a href="about" class="btn btn-primary btn-round btn-shine">read More</a></p>
				</div>
			</div>
			<!-- end of about link -->
		</div>
	</section>
	<!-- end of about section -->
