<?php 
    $sql = "SELECT * FROM album WHERE id != '' ";
    $query = $db->query($sql);
    if(mysqli_num_rows($query) > 0){
?>
	<!-- album section -->
	<section id="citsa-album" data-section="albums">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Albums</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
							<h3></h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-bottom-padded-sm">
				<div id="main">
					<div class="inner">
						<div class="columns">
                        <?php 
                            while($row = mysqli_fetch_array($query)){
                        ?>
                            <div class="image fit">
                                <a href="<?=$row['image'];?>" data-lightbox="example-set" data-title="<?=$row['title'];?>">
                                    <img src="<?=$row['image'];?>" alt="">
                                </a>
                            </div>
                        <?php
                        }
                        ?>
					</div>
				</div>
            </div>
			<div class="row">
				<div class="col-md-12 text-center to-animate">
					<p><a href="albums" class="btn  btn-primary btn-round btn-shine">See More</a></p>
				</div>
			</div>
		</div>
	</section>
	<!--end album section -->
<?php } ?>