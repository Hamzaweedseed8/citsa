<?php 
    $sql = "SELECT * FROM boards WHERE id != '' ";
    $query = $db->query($sql);
    if(mysqli_num_rows($query) > 0){
?>
    <!-- board section -->
	<section id="citsa-boards" data-section="boards">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Board Members</h2>
					<div class="row">
						<div class="col-md-8 col-md-offset-2 subtext to-animate">
							<h3></h3>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="swiper-container">
					<div class="swiper-wrapper">
						
                    <?php 
                        
                        while($row = mysqli_fetch_array($query)){
                    ?>
                        <div class="swiper-slide">
                            <div class="swipeImg">
                                <img src="<?=$row['image'];?>" alt="<?=$row['name'];?>">
                            </div>
                            <div class="details">
								<h3><?=$row['name'];?><br>
								<span><?=$row['position'];?></span><br>
								<span>0557566646</span>
							</h3>
                            </div>
                        </div>
                    <?php
                       }
                    ?>
					</div>
					<!-- Add Pagination -->
					<div class="swiper-pagination"></div>
				</div>
			</div>
			<!-- <div class="row">
				<div class="col-md-12 text-center to-animate">
					<p><a href="boards/index.html" class="btn btn-primary btn-round btn-shine">See More</a></p>
				</div>
			</div> -->
		</div>
	</section>
    <!-- End of board section -->
    
     <? }
?>

	

