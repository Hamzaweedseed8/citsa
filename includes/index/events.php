<?php 
    $sql = "SELECT * FROM events WHERE id != '' ORDER BY date_of_event LIMIT 5";
    $query = $db->query($sql);
    if(mysqli_num_rows($query) > 0){

        $single_sql= "SELECT * FROM events WHERE id != '' ORDER BY date_of_event  LIMIT 1";
        $single_query = $db->query($single_sql);
        $single = mysqli_fetch_array($single_query);
        $about_event = substr($single['event_about'], 0, 400);
?>

<!-- event section -->
<section id="citsa-testimonials" data-section="events">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">Events</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-lg-8">
					<div class="blog-posts-area">
						<!-- Single Featured Post -->
						<div class="single-blog-post featured-post single-post">
							<div class="post-thumb">
								<a href="events/details?detail=<?=$single['id'];?>">
									<?php 
										$photos = $single['image'];
										$photo = explode(',', $photos);
									 ?>
                                    <img src="<?=$photo[0];?>" alt="<?=$single['title'];?>">
                                </a>
							</div>
							<div class="post-data">
                                <span class="btn btn-date btn-default">
                                    <?=formatDateTime($single['date_of_event']);?>
                                </span>
                                <a href="events/details?detail=<?=$single['id'];?>" class="post-title">
                                    <h6><?=$single['title'];?></h6>
                                </a>
                                <div class="post-meta">
                                    <p class="post-author">Host <span><?=$single['host'];?></span></p>
                                    <p class="content">
                                        <?php 
                                            if(strlen($single['event_about']) <= 400){
												echo $about_event;
											}else{
                                                echo $about_event.' (...)';
                                            } 
                                        ?>
                                    </p>
                                </div>
                                <a href="events/details?detail=<?=$single['id'];?>" class="btn btn-readmore btn-round btn-shine"> Read More</a>
                            </div>
						</div>
                        
					</div>
				</div>

				<div class="col-12 col-lg-4">
					<div class="blog-sidebar-area">
						<!-- Latest Posts Widget -->
						<div class="latest-posts-widget mb-50">
							<!-- Backend: fetch from events table with limit of 8 in asc order  -->
                            <?php 
                            while($row = mysqli_fetch_array($query)){
                                if($row['id'] != $single['id']){
                        ?>
                            <!-- Single Featured Post -->
							<div class="single-blog-post small-featured-post d-flex">
								<div class="post-thumb">
									<a href="events/details?detail=<?=$row['id'];?>">
										<?php 
										$photos = $row['image'];
										$photo = explode(',', $photos);
									 ?>
                                        <img src="<?=$photo[0];?>" alt="<?=$row['title'];?>">
                                    </a>
								</div>
								<div class="post-data">
									<div class="post-meta">
										<a href="events/details?detail=<?=$row['id'];?>" class="post-title">
											<h6><?=$row['title'];?></h6>
										</a>
										<span class="d-flex">Host : <?=$row['host'];?></span>
										<p class="post-date"><span><?=formatTime($row['date_of_event']);?></span> | <span><?=formatMonthDay($row['date_of_event']);?></span></p>
									</div>
								</div>
							</div>
                            <?php 
                                    }
                                }
                            ?>
                            
						</div>                   
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end of event section -->

    <?php } ?>