<!-- Landing page -->
<section id="citsa-home" data-section="home" style="background-image: url(static/images/bg-img/11.jpg);">
		<!-- <div class="gradient"></div> -->
		<div class="container">
			<div class="text-wrap">
				<div class="text-inner">
					<div class="row">
						<div class="col-md-12 text-center">
						<!-- <div class="col-md-12 col-md-offset-2"> -->
							<!-- <h1 class="to-animate">Computer Science and Information technology Student Association.</h1> -->
							<h1 class="to-animate">Promoting Development Through Technology</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="slant"></div>
	</section>
	<!-- end of landing page -->