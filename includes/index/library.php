<!-- library section -->
<section id="citsa-library" data-section="library" style="background-image: url(static/images/bg-img/library.jpg);">
		<div class="slantOp"></div>
		<div class="gradient"></div>
		<div class="text-wrap">
			<div class="row">
				<div class="col-md-12 section-heading text-center" style="margin-bottom: 0px;">
					<h2 class="">Library</h2>
					<div class="row">
						<div class="">
							<h3>search for all book b</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<!-- seacrch box for library search-->
					<form action="library/index.php" method="GET">
						<div class="col-lg-3 col-md-3 col-xs-12"></div>
						<div class="col-md-6 InButn">
							<input type="text" name="library" id="library" class="typeahead tt-query " autocomplete="off" spellcheck="false"><button type="submit" class="btn btn-primary submit"><i class="icon-search"></i></button>
						</div>
						<div class="col-lg-3 col-md-3 col-xs-12"></div>
					</form>
					<!-- end of search box -->
				</div>
			</div>
		</div>
		<div class="slant"></div>
	</section>
	<!-- end of library section -->
