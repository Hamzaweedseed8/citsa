<header class="topnav">
		<div class="navbar-header">
			<span class="navbar-brand long_name"> <i class="icon-graduation-cap"></i> computer Science & information Technology Students Association (CITSA)</span><span class="navbar-brand short_name"> CITSA </span>
		</div>
		<!-- <nav>
			<ul>
				<li>
					<a href="login.html" class="btn btn-round btn-white btn-padding">Login</a>
				</li>
			</ul>
		</nav> -->
	</header>
	<header role="banner" id="citsa-header" >
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<!-- Mobile Toggle Menu Button -->
					<a href="#" class="js-citsa-nav-toggle citsa-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
					<a class="navbar-brand" href="index"><img src="static/images/logo/logo.png" alt="citsa"style="height: 60px;"></a> 
				</div>
				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="active" id="navbar_hide"><a href="#" data-nav-section="home"><span>Home</span></a></li>
						<li id="navbar_hide"><a href="#" data-nav-section="about"><span>About</span></a></li>
						<li id="navbar_hide"><a href="#" data-nav-section="library"><span>Library</span></a></li>
						<li id="navbar_hide"><a href="#" data-nav-section="news"><span>News</span></a></li>
						<li id="navbar_hide"><a href="#" data-nav-section="albums"><span>Albums</span></a></li>
						<li id="navbar_hide"><a href="#" data-nav-section="events"><span>Events</span></a></li>
						<li id="navbar_hide"><a href="#" data-nav-section="boards"><span>Boards</span></a></li>
						<li id="navbar_hide"><a href="#" data-nav-section="contact"><span>Contact</span></a></li>
					</ul>
				</div>
			</nav>
		</div>
	</header>
	<!-- end of main navigation -->
