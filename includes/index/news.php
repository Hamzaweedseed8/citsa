<?php 
    $sql = "SELECT * FROM new WHERE news_id != '' ORDER BY date_added DESC LIMIT 5";
    $query = $db->query($sql);
    if(mysqli_num_rows($query) > 0){

?>
    
    <!-- news section -->
	<section id="citsa-news" data-section="news">
		<div class="container">
			<div class="row">
				<div class="col-md-12 section-heading text-center">
					<h2 class="to-animate">News</h2>
				</div>
			</div>
			<div class="featured-post-area">
				<div class="row row-bottom-padded-sm">
					<div class="col-12 col-md-6 col-lg-8">
						<div class="row">
						<?php 
							$single_sql= "SELECT * FROM new WHERE news_id != '' ORDER BY date_added DESC  LIMIT 1";
							$single_query = $db->query($single_sql);
							$single = mysqli_fetch_array($single_query);
							$news_desc = substr($single['news_description'], 0, 400);
						?>
							<!-- main news post single -->
							<!--  Backend: fetch from news table limit of 1 . Make sure it the current news date or the latest new-->
							<div class="col-12 col-lg-12">
								<div class="single-blog-post featured-post to-animate">
									<div class="post-thumb">
										<a href="#"><img src="<?=$single['bg_img'];?>" alt=""></a>
									</div>
									<div class="post-data">
										<!-- <a href="#" class="post-catagory">Finance</a> -->
										<a href="#" class="post-title">
											<h6><?=$single['news_title'];?></h6>
										</a>
										<div class="post-meta">
											<p class="post-author">By <a href="#"><?=$single['news_reporter'];?></a></p>
											<p class="post-excerp">
                                                <?php 
                                                    if(strlen($single['news_description']) <= 400){
                                                        echo $news_desc;
                                                    }else{
                                                        echo $news_desc.' (...)';
                                                    } 
                                                    ?>
                                            </p>
											<!-- Post Like & Post Comment -->
											<!-- <div class="d-flex align-items-center">
												<a href="#" class="post-like"><img src="static/images/core-img/like.png" alt=""> <span>392</span></a>
												<a href="#" class="post-comment"><img src="static/images/core-img/chat.png" alt=""> <span>10</span></a>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-12 col-md-6 col-lg-4">
						<div class="section-heading-1 to-animate">
							<h6>Latest news</h6>
						</div>

						<!--  Backend: fetch from news table limit of 8 . Make sure it the current news date or the latest new . Please make sure that the id is not equall to news_id above-->
						<!-- Single Featured Post --->
						<div class="single-blog-post small-featured-post d-flex to-animate">
							<div class="post-thumb">
								<a href="#"><img src="/citsa-pwa/static/images/albums/4.jpg" alt=""></a>
							</div>
							<div class="post-data">
								<a href="#" class="post-catagory">Hamza</a>
								<div class="post-meta">
									<a href="#" class="post-title">
										<h6>Pellentesque mattis arcu massa, nec fringilla turpis eleifend id.</h6>
									</a>
									<p class="post-date"><span>7:00 AM</span> | <span>April 14</span></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
    <!-- end news section -->
    
    <?php } ?>