<header id="citsa-header1" class="navbar-fixed-top1">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <!--Mobile Toggle Menu Button -->
                    <a href="#" class="js-citsa-nav-toggle citsa-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
                    <a class="navbar-brand" href="/citsa-pwa/"><img src="/citsa-pwa/static/images/logo/logo.png" alt=""style="height: 60px;"></a>                 
                </div>
                <div  class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/citsa-pwa/index"><span>Home</span></a></li>
                        <li><a href="/citsa-pwa/about" ><span>About</span></a></li>
                        <li><a href="/citsa-pwa/library/index"><span>Library</span></a></li>
                        <li><a href="/citsa-pwa/news/index"><span>News</span></a></li>
                        <li><a href="/citsa-pwa/albums"><span>Albums</span></a></li>
                        <li><a href="/citsa-pwa/events/index"><span>Events</span></a></li>
                        <li><a href="/citsa-pwa/index#citsa-boards"><span>Boards</span></a></li>
                        <li><a href="/citsa-pwa/contact"><span>Contact</span></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>