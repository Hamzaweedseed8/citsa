<?php
if(isset($_REQUEST["file"])){
    // Get parameters
    $file = urldecode($_REQUEST["file"]); // Decode URL-encoded string
    $filepath = "../files/" . $file;
    
    // Process download
    if(file_exists($filepath)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($filepath));
        flush(); // Flush system output buffer
        readfile($filepath);
        exit;
    }
}

if(isset($_REQUEST["view"])){
    $viewname = urldecode($_REQUEST["view"]);
    $view_path = "../files/" . $viewname;
    if(file_exists($view_path)) {
    header('Content-type: application/pdf');
    header('Content-Disposition: inline; filename="' . $view_path . '"');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: ' . filesize($view_path));
    header('Accept-Ranges: bytes');
    @readfile($view_path);
    }
}
?>