<?php
    require_once '../core/db.php';
    include '../includes/head.php';
    include '../includes/nav.php';
?>

<!-- library section -->
<section id="citsa-library" data-section="library" style="background-image: url(/citsa-pwa/static/images/bg-img/library.jpg);">
    <div class="gradient" style=" z-index:0;"></div>
		<div class="text-wrap">
			<div class="row">
				<div class="col-md-12 section-heading text-center" style="margin-bottom: 0px;">
					<h2 class="">Library</h2>
					<div class="row">
						<div class="">
							<h3>search for all books</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<!-- seacrch box for library search-->
					<form action="index.php" method="GET">
						<div class="col-lg-3 col-md-3 col-xs-12"></div>
						<div class="col-md-6 InButn">
							<input type="text" name="library" id="library" class="typeahead tt-query " autocomplete="off" spellcheck="false"><button style="padding: 10px 20px;" type="submit" class="btn btn-primary submit"><i class="icon-search"></i></button>
						</div>
						<div class="col-lg-3 col-md-3 col-xs-12"></div>
					</form>
					<!-- end of search box -->
				</div>
			</div>
		</div>
		<div class="slant"></div>
	</section>
	<!-- end of library section -->
<?php

if(isset($_GET['library'])){    
    $query = $_GET['library']; 
    // $min_length = 3;
    // if(strlen($query) >= $min_length){
        $query = sanitize($query);
        $query = htmlspecialchars($query);
        $query = mysqli_real_escape_string($db,$query);
        $raw_results ="SELECT * FROM library WHERE (`keyword` LIKE '%".$query."%')" or die(mysqli_error());
        $run_query = $db->query($raw_results);
?>
        <section id="citsa-contact" data-section="">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center" >
                    <h3>Results on " <span style="color:#3f95ea ;font-style: italic;"><?=$query;?></span> "</h3>
                </div>
            </div>
                <div class="row row-bottom-padded-md">
                    <div class="col-md-12 library-result"> 
                        <ul>
                        <?php
                            if(mysqli_num_rows($run_query) > 0){
                                $no = 1;
                                while($results = mysqli_fetch_array($run_query)){
                                    $lib_download = urlencode($results['file']);
                        ?>
                            <li>
                                <span style='margin-right:20px;'><?=$no;?>.</span> 
                                <span class='filename'><?=$results['code'];?> -  <?=$results['title'];?></span>
                                <span class='pull-right'>
                                <a href='download.php?view=<?=$lib_download;?>' name="view file"><i class='icon-file'></i></a> 
                                <a href='download.php?file=<?=$lib_download;?>' name="download file"><i class='icon-download'></i></a>
                                </span>
                            </li>
                        <?php 
                            $no = $no + 1; 
                                } 
                            }else{ 
                               ?>
                                <li class="text-danger text-center"> 
                                    <span>No result on <?=$query;?></span>
                                </li>
                               <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        
<?php
    // }
    // else{ 
    //     echo "Minimum length is ".$min_length;
    // }
}
?>
<?php 
    include '../includes/footer.php';
    ?>