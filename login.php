<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Citsa</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Ubuntu" rel="stylesheet">
	<!-- Animate.css -->
	<link rel="stylesheet" href="static/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="static/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="static/css/bootstrap.css">
	<link rel="stylesheet" id="theme-switch" href="static/css/style.css">
	<link rel="stylesheet" type="text/css" href="static/css/reg.css">
	<script src="static/js/modernizr-2.6.2.min.js"></script>
	
</head>
<body>
	<section>
	<div class="bgPulse">
		<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
		<div class="container">
		<div class=" home">
			<div class="social social-circle">
			<li><a href="index.html" class="text-center"><i class="icon-home"  aria-hidden="true"></i></a></li>
				</div>
			</div>
			<div class="row">
				<div class="box">
					<form action="logindb.php" method="post" id="frmLogin">
	<div class="error-message">
		<?php if(isset($message)) { echo $message; } ?>	
	</div>	
	<div class="field-group">
		<div>
			<label for="login">Username</label>
		</div>
		<div>
			<input name="member_name" type="text" value="<?php if(isset($_COOKIE["member_login"])) { echo $_COOKIE["member_login"]; } ?>" class="input-field">
		</div>
	<div class="field-group">
		<div><label for="password">Password</label></div>
		<div><input name="member_password" type="password" value="" class="input-field"> 
	</div>
	<div class="field-group">
		<div><input type="checkbox" name="remember" id="remember" <?php if(isset($_COOKIE["member_login"])) { ?> checked 
			<?php } ?> />
		<label for="remember-me">Remember me</label>
	</div>
	<div class="field-group">
		<div><input type="submit" name="login" value="Login" class="form-submit-button"></span></div>
	</div>       
</form>
					<!-- <form id="myForm">
						<h1>Sign In</h1>
						<div class="inputBox form-group col-md-12">
							<input type="text" name="index_number" id="index_number" required="">
							<label>Index Number</label>
						</div>
						<div class="inputBox form-group col-md-12">
							<input type="password" name="password" id="password" required="">
							<label for="password">Password</label>
						</div>
						<div class="form-group col-md-12">
	                    	<h5><a href="forgot_password/index.html" class="text-primary pull-right" style="font-size: 16px;">Forgot Password?</a></h5>
	                    </div>
						<div class="form-group col-md-12">
							<input type="submit" value="Sign In" class="btn btn-primary btn-round btn-shine">
						</div>
					</form> -->
				</div>
			</div>
		</div>
	</div>
</section>
</body>
</html>